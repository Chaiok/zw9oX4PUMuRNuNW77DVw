var express = require("express");
var multer = require('multer');
var upload = multer();
var path = require("path");
var PORT = process.env.PORT || 3000;
var app = express();
var mysql = require("mysql2");

var http = require('http');
var server = http.Server(app);

const connection = mysql.createConnection({
    host: "db4free.net",
    user: "chaiok",
    database: "srprsmzfk",
    password: "44442222a"
});

connection.connect(function (err) {
    if (err) {
        return console.error("Ошибка: " + err.message);
    }
    else {
        console.log("Подключение к серверу MySQL успешно установлено");
    }
});

app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));

app.use("/form", express.static(__dirname + '/public'), function (request, response) {
    let form = "Заполните форму";
    if (request.query.false != undefined)
    {
        if(request.query.false=="name_zero")
        form = "Пожалуйста заполните имя";
        else if(request.query.false=="name")
        form = "Пожалуйста используйте только латинские символы";
        else if(request.query.false=="name_s")
        form = "Пожалуйста не используйте символы пустого пространства";
        else if(request.query.false=="email_zero")
        form = "Пожалуйста заполните email";
        else if(request.query.false=="email_@")
        form = "Пожалуйста не используйте больше одной @";
        else if(request.query.false=="email")
        form = "Пожалуйста используйте стандартные и латинские символы для заполнения почты";
        else if(request.query.false=="birthday_zero")
        form = "Пожалуйста выберите дату рождения";
        else if(request.query.false=="birthday")
        form = "Пожалуйста использйте следующий формат для д.р. 20-03-2007";
        else if(request.query.false=="sex_zero")
        form = "Пожалуйста выберите пол";
        else if(request.query.false=="sex")
        form = "male or female";
        else if(request.query.false=="limbs_zero")
        form = "Пожалуйста выберите количество конечностей";
        else if(request.query.false=="limbs")
        form = "Пожалуйста не используйте нестандартные символы";
        else if(request.query.false=="abilities_zero")
        form = "Пожалуйста выберите способности";
        else if(request.query.false=="abilities")
        form = "Пожалуйста используйте только латинские символы";
        else if(request.query.false=="biography")
        form = "Пожалуйста заполните биографию";
        else if(request.query.false=="checkbox")
        form = "Пожалуйста ознакомтись с контрактом";
        else if(request.query.false=="success")
        form = "Ваша форма успешно отправлена";
    }
    return response.render("form", {
        title: "Форма",
        form: form,
    });
});

app.get("/", function (request, response) {
    return response.send("Главная страница");
});

app.post("/create-form", upload.array(), function (request, response, next) {
    console.log(request.body);
    //name
    if (request.body.name == undefined)
        return response.redirect("/form?false=name_zero");
    if (request.body.name == '')
        return response.redirect("/form?false=name_zero");
    if (request.body.name.search(/\s/g) != -1)
        return response.redirect("/form?false=name_s");
    if (request.body.name.search(/[^A-Za-z]/g) != -1)
        return response.redirect("/form?false=name");
    //email
    if (request.body.email == undefined)
        return response.redirect("/form?false=email_zero");
    if (request.body.email == '')
        return response.redirect("/form?false=email_zero");
    if (request.body.email.search(/@{2}/g) != -1)
        return response.redirect("/form?false=email_@");
    if (request.body.email.search(/[^A-Za-z_]-/g) != -1)
        return response.redirect("/form?false=email");
    //birthday
    if (request.body.birthday == undefined)
        return response.redirect("/form?false=birthday_zero");
    if (request.body.birthday == '')
        return response.redirect("/form?false=birthday_zero");
    if (request.body.birthday.search(/[^0-9]-/g) != -1)
        return response.redirect("/form?false=birthday");
    //sex
    if (request.body.sex == undefined)
        return response.redirect("/form?false=sex_zero");
    if (request.body.sex != "male" && request.body.sex != "female")
        return response.redirect("/form?false=sex");
    //limbs
    if (request.body.limbs == undefined)
        return response.redirect("/form?false=limbs_zero");
    if (request.body.limbs.search(/[^A-Za-z0-9]/g) != -1)
        return response.redirect("/form?false=limbs");
    //abilities
    if (request.body.abilities == undefined)
        return response.redirect("/form?false=abilities_zero");
    if (Array.isArray(request.body.abilities))
        request.body.abilities.forEach(function (item, index, array) {
            if (item.search(/[^A-Za-z]/g) != -1)
                return response.redirect("/form?false=abilities");
        });
    else {
        if (request.body.abilities.search(/[^A-Za-z]/g) != -1)
            return response.redirect("/form?false=abilities");
    }
    //biography
    if (request.body.biography == undefined)
        return response.redirect("/form?false=biography");
    if (request.body.biography == '')
        return response.redirect("/form?false=biography");
    //checkbox
    if (request.body.checkbox == undefined)
        return response.redirect("/form?false=checkbox");
    if (request.body.checkbox != 'on')
        return response.redirect("/form?false=checkbox");

    // execute will internally call prepare and query
    let json = JSON.stringify(request.body.abilities);
    connection.execute(
        "INSERT INTO tableform(name,email,birthday,sex,limbs,abilities,biography,checkbox) VALUES (?,?,?,?,?,?,?,?)",
        [request.body.name, request.body.email, request.body.birthday, request.body.sex, request.body.limbs, json, request.body.biography, request.body.checkbox],
        function (err, results, fields) {
            console.log(results);
            console.log(fields);
            if (err) {
                console.log("Ошибка: " + err.message);
            }
        }
    );
    return response.redirect("/form?false=success");
});

server.listen(PORT, function () {
});