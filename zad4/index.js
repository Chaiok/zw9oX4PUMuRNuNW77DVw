var PORT = process.env.PORT || 3000;
var mysql = require("mysql2");
var http = require('http');
var ejs = require('ejs');
var fs = require('fs');
var qs = require('querystring');
var cookie = require('cookie');

const connection = mysql.createConnection({
    host: "db4free.net",
    user: "chaiok",
    database: "srprsmzfk",
    password: "44442222a"
});

connection.connect(function (err) {
    if (err) {
        return console.error("Ошибка: " + err.message);
    }
    else {
        console.log("Подключение к серверу MySQL успешно установлено");
    }
});

function processPost(request, response, callback) {
    var queryData = "";
    if (typeof callback !== 'function')
        return null;
    if (request.method == 'POST') {
        request.on('data', function (data) {
            queryData += data;
            if (queryData.length > 1e6) {
                queryData = "";
                response.writeHead(413, { 'Content-Type': 'text/plain' }).end();
                request.connection.destroy();
            }
        });
        request.on('end', function () {
            request.post = qs.parse(queryData);
            callback();
        });
    } else {
        response.writeHead(405, { 'Content-Type': 'text/plain' });
        response.end();
    }
}

const server = http.createServer(function (request, response) {
    //var cookies = ['yummy_cookie=aaa','tasty_cookie=sss'];
    if (request.method == 'POST') {
        processPost(request, response, function () {
            console.log("request.post:");
            console.log(request.post);
            // Use request.post here
            var cookies = {};
            if (request.headers.cookie !== undefined) {
                cookies = cookie.parse(request.headers.cookie);
            }
            //response.setHeader('Set-Cookie', cookie.serialize('name', "Vasy", {
            //    httpOnly: true,
            //    maxAge: 60 * 60 * 24 * 7 // 1 week
            //}));
            //name
            if (request.post.name == undefined)
                cookies['name_fail'] = 'name_zero';
            else if (request.post.name == '') {
                cookies['name_fail'] = 'name_zero';
                cookies['name'] = encodeURIComponent(request.post.name)
            }
            //else if (request.post.name.search(/\s/g) != -1)
            //    cookies['name_fail'] = 'name_zero';
            else if (request.post.name.search(/[^A-Za-z]/g) != -1) {
                cookies['name_fail'] = 'name';
                cookies['name'] = encodeURIComponent(request.post.name)
            }
            else {
                cookies['name_fail'] = '';
                cookies['name'] = encodeURIComponent(request.post.name)
            }
            //email
            if (request.post.email == undefined)
                cookies['email_fail'] = 'email_zero';
            else if (request.post.email == '') {
                cookies['email_fail'] = 'email_zero';
                cookies['email'] = request.post.email;
            }
            else if (!(/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/.test(request.post.email))) {
                cookies['email_fail'] = 'email';
                cookies['email'] = request.post.email;
            }
            else {
                cookies['email_fail'] = '';
                cookies['email'] = request.post.email;
            }
            //birthday
            if (request.post.birthday == undefined)
                cookies['birthday_fail'] = 'birthday_zero';
            else if (request.post.birthday == '') {
                cookies['birthday_fail'] = 'birthday_zero';
                cookies['birthday'] = request.post.birthday;
            }
            else if (request.post.birthday.search(/[^0-9]-/g) != -1) {
                cookies['birthday_fail'] = 'birthday';
                cookies['birthday'] = request.post.birthday;
            }
            else {
                cookies['birthday_fail'] = '';
                cookies['birthday'] = request.post.birthday;
            }
            //sex
            if (request.post.sex == undefined)
                cookies['sex_fail'] = 'sex_zero';
            else if (request.post.sex != "male" && request.post.sex != "female") {
                cookies['sex_fail'] = 'sex';
                cookies['sex'] = request.post.sex;
            }
            else {
                cookies['sex_fail'] = '';
                cookies['sex'] = request.post.sex;
            }
            //limbs
            if (request.post.limbs == undefined)
                cookies['limbs_fail'] = 'limbs_zero';
            else if (request.post.limbs.search(/[^A-Za-z0-9]/g) != -1) {
                cookies['limbs_fail'] = 'limbs';
                cookies['limbs'] = request.post.limbs;
            }
            else {
                cookies['limbs_fail'] = '';
                cookies['limbs'] = request.post.limbs;
            }
            //abilities
            if (request.post.abilities == undefined)
                cookies['abilities_fail'] = 'abilities_zero';
            else if (Array.isArray(request.post.abilities)) {
                cookies['abilities_fail'] = '';
                request.post.abilities.forEach(function (item, index, array) {
                    if (item == '')
                        cookies['abilities_fail'] = 'abilities';
                    if (item.search(/[^A-Za-z]/g) != -1)
                        cookies['abilities_fail'] = 'abilities';
                });
                cookies['abilities'] = request.post.abilities;
            }
            else {
                cookies['abilities_fail'] = '';
                cookies['abilities'] = request.post.abilities;
                if (request.post.abilities == '') {
                    cookies['abilities_fail'] = 'abilities';
                }
                else if (request.post.abilities.search(/[^A-Za-z]/g) != -1) {
                    cookies['abilities_fail'] = 'abilities';
                }
            }
            //biography
            if (request.post.biography == undefined) {
                cookies['biography_fail'] = 'biography_zero';
            }
            else if (request.post.biography == '') {
                cookies['biography_fail'] = 'biography_zero';
                cookies['biography'] = encodeURIComponent(request.post.biography);
            }
            else {
                cookies['biography_fail'] = '';
                cookies['biography'] = encodeURIComponent(request.post.biography);
            }
            //checkbox
            if (request.post.checkbox == undefined)
                cookies['checkbox_fail'] = 'checkbox_zero';
            else if (request.post.checkbox != 'on') {
                cookies['checkbox_fail'] = 'checkbox';
                cookies['checkbox'] = request.post.checkbox;
            }
            else {
                cookies['checkbox_fail'] = '';
                cookies['checkbox'] = request.post.checkbox;
            }
            console.log('test');
            console.log(cookies);
            var testcooook = [];
            if (cookies['name_fail'] == '' && cookies['email_fail'] == '' && cookies['birthday_fail'] == '' && cookies['sex_fail'] == '' && cookies['limbs_fail'] == '' && cookies['abilities_fail'] == '' && cookies['biography_fail'] == '' && cookies['checkbox_fail'] == '') {
                var success_cookies = {};
                success_cookies['success_cookies'] = 'true' + '; expires=' + new Date(new Date().getTime() + (86409000 * 365.24)).toUTCString();
                success_cookies['success_name'] = request.post.name + '; expires=' + new Date(new Date().getTime() + (86409000 * 365.24)).toUTCString();
                success_cookies['success_email'] = request.post.email + '; expires=' + new Date(new Date().getTime() + (86409000 * 365.24)).toUTCString();
                success_cookies['success_birthday'] = request.post.birthday + '; expires=' + new Date(new Date().getTime() + (86409000 * 365.24)).toUTCString();
                success_cookies['success_sex'] = request.post.sex + '; expires=' + new Date(new Date().getTime() + (86409000 * 365.24)).toUTCString();
                success_cookies['success_limbs'] = request.post.limbs + '; expires=' + new Date(new Date().getTime() + (86409000 * 365.24)).toUTCString();
                success_cookies['success_abilities'] = request.post.abilities + '; expires=' + new Date(new Date().getTime() + (86409000 * 365.24)).toUTCString();
                success_cookies['success_biography'] = encodeURIComponent(request.post.biography) + '; expires=' + new Date(new Date().getTime() + (86409000 * 365.24)).toUTCString();
                success_cookies['success_checkbox'] = request.post.checkbox + '; expires=' + new Date(new Date().getTime() + (86409000 * 365.24)).toUTCString();
                for (var [key, value] of Object.entries(success_cookies)) {
                    testcooook.push(String([key + '=' + value]));
                }
                success_cookies = testcooook;
                console.log('success_cookies');
                console.log(success_cookies);
                let json = JSON.stringify(request.post.abilities);
                connection.execute(
                    "INSERT INTO tableform(name,email,birthday,sex,limbs,abilities,biography,checkbox) VALUES (?,?,?,?,?,?,?,?)",
                    [request.post.name, request.post.email, request.post.birthday, request.post.sex, request.post.limbs, json, request.post.biography, request.post.checkbox],
                    function (err, results, fields) {
                        console.log(results);
                        console.log(fields);
                        if (err) {
                            console.log("Ошибка: " + err.message);
                        }
                    }
                );
                response.writeHead(200, {
                    "Content-Type": "text/html",
                    'Set-Cookie': success_cookies
                });
            }
            else {
                delete cookies['success_name'];
                delete cookies['success_email'];
                delete cookies['success_birthday'];
                delete cookies['success_sex'];
                delete cookies['success_limbs'];
                delete cookies['success_abilities'];
                delete cookies['success_biography'];
                delete cookies['success_checkbox'];
                for (var [key, value] of Object.entries(cookies)) {
                    testcooook.push(String([key + '=' + value]));
                }
                cookies = testcooook;
                response.writeHead(200, {
                    "Content-Type": "text/html",
                    'Set-Cookie': cookies,
                });
            }
            var htmlContent = fs.readFileSync(__dirname + '/views/form.ejs', 'utf8');
            var htmlRenderized = ejs.render(htmlContent, { filename: 'form.ejs', title: "Форма", form: "Форма", });
            response.write(htmlRenderized);
            response.end();
        });
    } else {
        var htmlContent = fs.readFileSync(__dirname + '/views/form.ejs', 'utf8');
        var htmlRenderized = ejs.render(htmlContent, { filename: 'form.ejs', title: "Форма", form: "Форма", });
        response.writeHead(200, {
            "Content-Type": "text/html",
        });
        response.write(htmlRenderized);
        response.end();
    }
})

server.listen(PORT, function (error) {
    if (error) {
        console.log("Wrong: " + error)
    }
    else {
        console.log("Server listening port: " + PORT)
    }
});